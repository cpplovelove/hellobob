from flask import Flask,request

class api:
    def add(self, x, y):
        print("x+y=",(x+y))
        return (x+y)

    def sub(self, x, y):
        print("x-y=",(x-y))
        return (x-y)


app = Flask(__name__)
@app.route('/')
def index():
    return 'Hello Bob from 선용이'

@app.route('/add')
def add():
    args = request.args
    a=int(args["a"])
    b=int(args["b"])
    return api.add(a,b)

@app.route('/sub')
def sub():
    args = request.args
    a=int(args["a"])
    b=int(args["b"])
    return api.sub(a,b)


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=8057)
