
import unittest
from app import api

class test_app(unittest.TestCase):
    def test_add(self):
        instance = api()
        result = instance.add(1,2)
        self.assertEqual(result, 3)

    def test_sub(self):
        instance = api()
        result = instance.sub(3,5)
        self.assertEqual(result, -2)

if __name__ == "__main__":
    unittest.main()